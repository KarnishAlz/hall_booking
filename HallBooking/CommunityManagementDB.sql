USE [master]
GO
/****** Object:  Database [CommunityManagementDB]    Script Date: 2/24/2020 11:35:46 PM ******/
CREATE DATABASE [CommunityManagementDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CommunityManagementDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\CommunityManagementDB.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CommunityManagementDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\CommunityManagementDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CommunityManagementDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CommunityManagementDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CommunityManagementDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [CommunityManagementDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CommunityManagementDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CommunityManagementDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CommunityManagementDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CommunityManagementDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CommunityManagementDB] SET  MULTI_USER 
GO
ALTER DATABASE [CommunityManagementDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CommunityManagementDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CommunityManagementDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CommunityManagementDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [CommunityManagementDB]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 2/24/2020 11:35:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Admin](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Admin] [varchar](max) NOT NULL,
	[Password] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Booking]    Script Date: 2/24/2020 11:35:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Booking](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Time] [varchar](50) NOT NULL,
	[BookingDate] [date] NOT NULL,
	[TotalAmount] [int] NOT NULL,
	[PaidAmount] [int] NOT NULL,
	[DueAmount] [int] NOT NULL,
	[CustomerName] [varchar](max) NOT NULL,
	[CustomerPhone] [varchar](max) NOT NULL,
	[CustomerEmail] [varchar](max) NULL,
	[Purpose] [varchar](50) NOT NULL,
	[Status] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Booking_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Expense]    Script Date: 2/24/2020 11:35:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Expense](
	[Id] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[Amount] [money] NOT NULL,
	[Description] [varchar](max) NULL,
	[Title] [varchar](max) NOT NULL,
 CONSTRAINT [PK__Expense__3214EC076C266FF5] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ManagerCreate]    Script Date: 2/24/2020 11:35:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ManagerCreate](
	[Name] [varchar](max) NOT NULL,
	[Position] [varchar](max) NULL,
	[Email] [varchar](max) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[JoiningDate] [date] NOT NULL,
 CONSTRAINT [PK_ManagerCreate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Setting]    Script Date: 2/24/2020 11:35:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Setting](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Image] [varchar](max) NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([Id], [Admin], [Password]) VALUES (1, N'Admin', N'admin')
SET IDENTITY_INSERT [dbo].[Admin] OFF
SET IDENTITY_INSERT [dbo].[Booking] ON 

INSERT [dbo].[Booking] ([Id], [Time], [BookingDate], [TotalAmount], [PaidAmount], [DueAmount], [CustomerName], [CustomerPhone], [CustomerEmail], [Purpose], [Status]) VALUES (2033, N'Day', CAST(0xBF400B00 AS Date), 20000, 10000, 10000, N'ABC', N'4444', N'abc@abc.com', N'Birthday', N'Pending')
SET IDENTITY_INSERT [dbo].[Booking] OFF
INSERT [dbo].[Expense] ([Id], [Date], [Amount], [Description], [Title]) VALUES (0, CAST(0xBE400B00 AS Date), 200.0000, N'', N'Gas Bill')
SET IDENTITY_INSERT [dbo].[Setting] ON 

INSERT [dbo].[Setting] ([Id], [Image], [Name]) VALUES (1, NULL, N'Hall Booking')
SET IDENTITY_INSERT [dbo].[Setting] OFF
ALTER TABLE [dbo].[Setting] ADD  CONSTRAINT [DF_Setting_Name]  DEFAULT ('Hall Booking') FOR [Name]
GO
USE [master]
GO
ALTER DATABASE [CommunityManagementDB] SET  READ_WRITE 
GO
