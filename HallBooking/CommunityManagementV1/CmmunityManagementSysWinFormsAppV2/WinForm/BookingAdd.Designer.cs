﻿namespace CmmunityManagementSysWinFormsAppV2.WinForm
{
    partial class BookingAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.errorIconPictureBox = new FontAwesome.Sharp.IconPictureBox();
            this.errorlabel = new System.Windows.Forms.Label();
            this.exitButton = new FontAwesome.Sharp.IconPictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.customerMailTextBox = new MetroFramework.Controls.MetroTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.customerPhoneTextBox = new MetroFramework.Controls.MetroTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.customerNameTextBox = new MetroFramework.Controls.MetroTextBox();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.totalTextBox = new MetroFramework.Controls.MetroTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.paidTextBox = new MetroFramework.Controls.MetroTextBox();
            this.dueTextBox = new MetroFramework.Controls.MetroTextBox();
            this.saveButton = new MetroFramework.Controls.MetroButton();
            this.deleteButton = new MetroFramework.Controls.MetroButton();
            this.dateTimePicker1 = new MetroFramework.Controls.MetroDateTime();
            this.nightRadioButton = new MetroFramework.Controls.MetroRadioButton();
            this.dayRadioButton = new MetroFramework.Controls.MetroRadioButton();
            this.purposeComboBox = new MetroFramework.Controls.MetroComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorIconPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).BeginInit();
            this.metroPanel3.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.BackColor = System.Drawing.Color.Transparent;
            this.metroPanel1.Controls.Add(this.errorIconPictureBox);
            this.metroPanel1.Controls.Add(this.errorlabel);
            this.metroPanel1.Controls.Add(this.exitButton);
            this.metroPanel1.Controls.Add(this.label8);
            this.metroPanel1.Controls.Add(this.metroPanel3);
            this.metroPanel1.Controls.Add(this.metroPanel2);
            this.metroPanel1.Controls.Add(this.saveButton);
            this.metroPanel1.Controls.Add(this.deleteButton);
            this.metroPanel1.Controls.Add(this.dateTimePicker1);
            this.metroPanel1.Controls.Add(this.nightRadioButton);
            this.metroPanel1.Controls.Add(this.dayRadioButton);
            this.metroPanel1.Controls.Add(this.purposeComboBox);
            this.metroPanel1.Controls.Add(this.label5);
            this.metroPanel1.Controls.Add(this.label4);
            this.metroPanel1.Controls.Add(this.label3);
            this.metroPanel1.Controls.Add(this.label2);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(0, 0);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(820, 486);
            this.metroPanel1.TabIndex = 39;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // errorIconPictureBox
            // 
            this.errorIconPictureBox.BackColor = System.Drawing.Color.White;
            this.errorIconPictureBox.ForeColor = System.Drawing.Color.Red;
            this.errorIconPictureBox.IconChar = FontAwesome.Sharp.IconChar.Exclamation;
            this.errorIconPictureBox.IconColor = System.Drawing.Color.Red;
            this.errorIconPictureBox.IconSize = 30;
            this.errorIconPictureBox.Location = new System.Drawing.Point(7, 451);
            this.errorIconPictureBox.Name = "errorIconPictureBox";
            this.errorIconPictureBox.Size = new System.Drawing.Size(30, 30);
            this.errorIconPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.errorIconPictureBox.TabIndex = 67;
            this.errorIconPictureBox.TabStop = false;
            this.errorIconPictureBox.Visible = false;
            // 
            // errorlabel
            // 
            this.errorlabel.AutoSize = true;
            this.errorlabel.Font = new System.Drawing.Font("Yu Gothic UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorlabel.ForeColor = System.Drawing.Color.DarkGray;
            this.errorlabel.Location = new System.Drawing.Point(43, 456);
            this.errorlabel.Name = "errorlabel";
            this.errorlabel.Size = new System.Drawing.Size(133, 25);
            this.errorlabel.TabIndex = 66;
            this.errorlabel.Text = "Error Message";
            this.errorlabel.Visible = false;
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.Transparent;
            this.exitButton.ForeColor = System.Drawing.Color.Black;
            this.exitButton.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.exitButton.IconColor = System.Drawing.Color.Black;
            this.exitButton.IconSize = 19;
            this.exitButton.Location = new System.Drawing.Point(677, 0);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(20, 19);
            this.exitButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.exitButton.TabIndex = 65;
            this.exitButton.TabStop = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(61, 251);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 25);
            this.label8.TabIndex = 64;
            this.label8.Text = "Customer";
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.customerMailTextBox);
            this.metroPanel3.Controls.Add(this.label12);
            this.metroPanel3.Controls.Add(this.label11);
            this.metroPanel3.Controls.Add(this.customerPhoneTextBox);
            this.metroPanel3.Controls.Add(this.label10);
            this.metroPanel3.Controls.Add(this.customerNameTextBox);
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(177, 251);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(492, 139);
            this.metroPanel3.TabIndex = 63;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // customerMailTextBox
            // 
            // 
            // 
            // 
            this.customerMailTextBox.CustomButton.Image = null;
            this.customerMailTextBox.CustomButton.Location = new System.Drawing.Point(225, 1);
            this.customerMailTextBox.CustomButton.Name = "";
            this.customerMailTextBox.CustomButton.Size = new System.Drawing.Size(31, 31);
            this.customerMailTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.customerMailTextBox.CustomButton.TabIndex = 1;
            this.customerMailTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.customerMailTextBox.CustomButton.UseSelectable = true;
            this.customerMailTextBox.CustomButton.Visible = false;
            this.customerMailTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.customerMailTextBox.Lines = new string[0];
            this.customerMailTextBox.Location = new System.Drawing.Point(133, 91);
            this.customerMailTextBox.MaxLength = 32767;
            this.customerMailTextBox.Multiline = true;
            this.customerMailTextBox.Name = "customerMailTextBox";
            this.customerMailTextBox.PasswordChar = '\0';
            this.customerMailTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.customerMailTextBox.SelectedText = "";
            this.customerMailTextBox.SelectionLength = 0;
            this.customerMailTextBox.SelectionStart = 0;
            this.customerMailTextBox.ShortcutsEnabled = true;
            this.customerMailTextBox.Size = new System.Drawing.Size(257, 33);
            this.customerMailTextBox.TabIndex = 59;
            this.customerMailTextBox.UseSelectable = true;
            this.customerMailTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.customerMailTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(52, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 25);
            this.label12.TabIndex = 47;
            this.label12.Text = "Name";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(52, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 25);
            this.label11.TabIndex = 48;
            this.label11.Text = "Phone";
            // 
            // customerPhoneTextBox
            // 
            // 
            // 
            // 
            this.customerPhoneTextBox.CustomButton.Image = null;
            this.customerPhoneTextBox.CustomButton.Location = new System.Drawing.Point(225, 1);
            this.customerPhoneTextBox.CustomButton.Name = "";
            this.customerPhoneTextBox.CustomButton.Size = new System.Drawing.Size(31, 31);
            this.customerPhoneTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.customerPhoneTextBox.CustomButton.TabIndex = 1;
            this.customerPhoneTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.customerPhoneTextBox.CustomButton.UseSelectable = true;
            this.customerPhoneTextBox.CustomButton.Visible = false;
            this.customerPhoneTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.customerPhoneTextBox.Lines = new string[0];
            this.customerPhoneTextBox.Location = new System.Drawing.Point(133, 52);
            this.customerPhoneTextBox.MaxLength = 32767;
            this.customerPhoneTextBox.Multiline = true;
            this.customerPhoneTextBox.Name = "customerPhoneTextBox";
            this.customerPhoneTextBox.PasswordChar = '\0';
            this.customerPhoneTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.customerPhoneTextBox.SelectedText = "";
            this.customerPhoneTextBox.SelectionLength = 0;
            this.customerPhoneTextBox.SelectionStart = 0;
            this.customerPhoneTextBox.ShortcutsEnabled = true;
            this.customerPhoneTextBox.Size = new System.Drawing.Size(257, 33);
            this.customerPhoneTextBox.TabIndex = 58;
            this.customerPhoneTextBox.UseSelectable = true;
            this.customerPhoneTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.customerPhoneTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(52, 99);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 25);
            this.label10.TabIndex = 49;
            this.label10.Text = "Email";
            // 
            // customerNameTextBox
            // 
            // 
            // 
            // 
            this.customerNameTextBox.CustomButton.Image = null;
            this.customerNameTextBox.CustomButton.Location = new System.Drawing.Point(225, 1);
            this.customerNameTextBox.CustomButton.Name = "";
            this.customerNameTextBox.CustomButton.Size = new System.Drawing.Size(31, 31);
            this.customerNameTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.customerNameTextBox.CustomButton.TabIndex = 1;
            this.customerNameTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.customerNameTextBox.CustomButton.UseSelectable = true;
            this.customerNameTextBox.CustomButton.Visible = false;
            this.customerNameTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.customerNameTextBox.Lines = new string[0];
            this.customerNameTextBox.Location = new System.Drawing.Point(133, 13);
            this.customerNameTextBox.MaxLength = 32767;
            this.customerNameTextBox.Multiline = true;
            this.customerNameTextBox.Name = "customerNameTextBox";
            this.customerNameTextBox.PasswordChar = '\0';
            this.customerNameTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.customerNameTextBox.SelectedText = "";
            this.customerNameTextBox.SelectionLength = 0;
            this.customerNameTextBox.SelectionStart = 0;
            this.customerNameTextBox.ShortcutsEnabled = true;
            this.customerNameTextBox.Size = new System.Drawing.Size(257, 33);
            this.customerNameTextBox.TabIndex = 57;
            this.customerNameTextBox.UseSelectable = true;
            this.customerNameTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.customerNameTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.label7);
            this.metroPanel2.Controls.Add(this.label1);
            this.metroPanel2.Controls.Add(this.totalTextBox);
            this.metroPanel2.Controls.Add(this.label6);
            this.metroPanel2.Controls.Add(this.paidTextBox);
            this.metroPanel2.Controls.Add(this.dueTextBox);
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(177, 161);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(492, 72);
            this.metroPanel2.TabIndex = 62;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label7.Location = new System.Drawing.Point(329, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 25);
            this.label7.TabIndex = 58;
            this.label7.Text = "Due";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.label1.Location = new System.Drawing.Point(172, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 25);
            this.label1.TabIndex = 57;
            this.label1.Text = "Paid";
            // 
            // totalTextBox
            // 
            // 
            // 
            // 
            this.totalTextBox.CustomButton.Image = null;
            this.totalTextBox.CustomButton.Location = new System.Drawing.Point(62, 1);
            this.totalTextBox.CustomButton.Name = "";
            this.totalTextBox.CustomButton.Size = new System.Drawing.Size(31, 31);
            this.totalTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.totalTextBox.CustomButton.TabIndex = 1;
            this.totalTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.totalTextBox.CustomButton.UseSelectable = true;
            this.totalTextBox.CustomButton.Visible = false;
            this.totalTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.totalTextBox.Lines = new string[0];
            this.totalTextBox.Location = new System.Drawing.Point(64, 14);
            this.totalTextBox.MaxLength = 32767;
            this.totalTextBox.Multiline = true;
            this.totalTextBox.Name = "totalTextBox";
            this.totalTextBox.PasswordChar = '\0';
            this.totalTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.totalTextBox.SelectedText = "";
            this.totalTextBox.SelectionLength = 0;
            this.totalTextBox.SelectionStart = 0;
            this.totalTextBox.ShortcutsEnabled = true;
            this.totalTextBox.Size = new System.Drawing.Size(94, 33);
            this.totalTextBox.TabIndex = 54;
            this.totalTextBox.UseSelectable = true;
            this.totalTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.totalTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.totalTextBox.TextChanged += new System.EventHandler(this.totalTextBox_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 25);
            this.label6.TabIndex = 43;
            this.label6.Text = "Total";
            // 
            // paidTextBox
            // 
            // 
            // 
            // 
            this.paidTextBox.CustomButton.Image = null;
            this.paidTextBox.CustomButton.Location = new System.Drawing.Point(62, 1);
            this.paidTextBox.CustomButton.Name = "";
            this.paidTextBox.CustomButton.Size = new System.Drawing.Size(31, 31);
            this.paidTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.paidTextBox.CustomButton.TabIndex = 1;
            this.paidTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.paidTextBox.CustomButton.UseSelectable = true;
            this.paidTextBox.CustomButton.Visible = false;
            this.paidTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.paidTextBox.Lines = new string[0];
            this.paidTextBox.Location = new System.Drawing.Point(229, 14);
            this.paidTextBox.MaxLength = 32767;
            this.paidTextBox.Multiline = true;
            this.paidTextBox.Name = "paidTextBox";
            this.paidTextBox.PasswordChar = '\0';
            this.paidTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.paidTextBox.SelectedText = "";
            this.paidTextBox.SelectionLength = 0;
            this.paidTextBox.SelectionStart = 0;
            this.paidTextBox.ShortcutsEnabled = true;
            this.paidTextBox.Size = new System.Drawing.Size(94, 33);
            this.paidTextBox.TabIndex = 55;
            this.paidTextBox.UseSelectable = true;
            this.paidTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.paidTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.paidTextBox.TextChanged += new System.EventHandler(this.paidTextBox_TextChanged);
            // 
            // dueTextBox
            // 
            // 
            // 
            // 
            this.dueTextBox.CustomButton.Image = null;
            this.dueTextBox.CustomButton.Location = new System.Drawing.Point(62, 1);
            this.dueTextBox.CustomButton.Name = "";
            this.dueTextBox.CustomButton.Size = new System.Drawing.Size(31, 31);
            this.dueTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.dueTextBox.CustomButton.TabIndex = 1;
            this.dueTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.dueTextBox.CustomButton.UseSelectable = true;
            this.dueTextBox.CustomButton.Visible = false;
            this.dueTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.dueTextBox.Lines = new string[0];
            this.dueTextBox.Location = new System.Drawing.Point(383, 14);
            this.dueTextBox.MaxLength = 32767;
            this.dueTextBox.Multiline = true;
            this.dueTextBox.Name = "dueTextBox";
            this.dueTextBox.PasswordChar = '\0';
            this.dueTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dueTextBox.SelectedText = "";
            this.dueTextBox.SelectionLength = 0;
            this.dueTextBox.SelectionStart = 0;
            this.dueTextBox.ShortcutsEnabled = true;
            this.dueTextBox.Size = new System.Drawing.Size(94, 33);
            this.dueTextBox.TabIndex = 56;
            this.dueTextBox.UseSelectable = true;
            this.dueTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.dueTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.Color.Transparent;
            this.saveButton.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.saveButton.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.saveButton.Location = new System.Drawing.Point(388, 396);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(112, 47);
            this.saveButton.TabIndex = 61;
            this.saveButton.Text = "Save";
            this.saveButton.UseSelectable = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.Transparent;
            this.deleteButton.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.deleteButton.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.deleteButton.Location = new System.Drawing.Point(521, 396);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(107, 47);
            this.deleteButton.TabIndex = 60;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseSelectable = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(165, 101);
            this.dateTimePicker1.MinimumSize = new System.Drawing.Size(0, 29);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 29);
            this.dateTimePicker1.TabIndex = 53;
            this.dateTimePicker1.Value = new System.DateTime(2020, 2, 16, 0, 0, 0, 0);
            // 
            // nightRadioButton
            // 
            this.nightRadioButton.AutoSize = true;
            this.nightRadioButton.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.nightRadioButton.Location = new System.Drawing.Point(261, 70);
            this.nightRadioButton.Name = "nightRadioButton";
            this.nightRadioButton.Size = new System.Drawing.Size(72, 25);
            this.nightRadioButton.TabIndex = 52;
            this.nightRadioButton.Text = "Night";
            this.nightRadioButton.UseSelectable = true;
            // 
            // dayRadioButton
            // 
            this.dayRadioButton.AutoSize = true;
            this.dayRadioButton.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.dayRadioButton.Location = new System.Drawing.Point(165, 70);
            this.dayRadioButton.Name = "dayRadioButton";
            this.dayRadioButton.Size = new System.Drawing.Size(59, 25);
            this.dayRadioButton.TabIndex = 51;
            this.dayRadioButton.Text = "Day";
            this.dayRadioButton.UseSelectable = true;
            // 
            // purposeComboBox
            // 
            this.purposeComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.purposeComboBox.FormattingEnabled = true;
            this.purposeComboBox.ItemHeight = 29;
            this.purposeComboBox.Items.AddRange(new object[] {
            "Marraige",
            "Birthday",
            "Corporate Function",
            "Others"});
            this.purposeComboBox.Location = new System.Drawing.Point(165, 22);
            this.purposeComboBox.Name = "purposeComboBox";
            this.purposeComboBox.Size = new System.Drawing.Size(244, 35);
            this.purposeComboBox.TabIndex = 50;
            this.purposeComboBox.UseSelectable = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(75, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 25);
            this.label5.TabIndex = 42;
            this.label5.Text = "Amount";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(93, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 25);
            this.label4.TabIndex = 41;
            this.label4.Text = "Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(91, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 25);
            this.label3.TabIndex = 40;
            this.label3.Text = "Time";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(63, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 25);
            this.label2.TabIndex = 39;
            this.label2.Text = "Purpose";
            // 
            // BookingAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(832, 504);
            this.Controls.Add(this.metroPanel1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BookingAdd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Booking";
            this.Load += new System.EventHandler(this.Booking_Load);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorIconPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).EndInit();
            this.metroPanel3.ResumeLayout(false);
            this.metroPanel3.PerformLayout();
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroButton saveButton;
        private MetroFramework.Controls.MetroButton deleteButton;
        private MetroFramework.Controls.MetroTextBox customerMailTextBox;
        private MetroFramework.Controls.MetroTextBox customerPhoneTextBox;
        private MetroFramework.Controls.MetroTextBox customerNameTextBox;
        private MetroFramework.Controls.MetroTextBox dueTextBox;
        private MetroFramework.Controls.MetroTextBox paidTextBox;
        private MetroFramework.Controls.MetroTextBox totalTextBox;
        private MetroFramework.Controls.MetroDateTime dateTimePicker1;
        private MetroFramework.Controls.MetroRadioButton nightRadioButton;
        private MetroFramework.Controls.MetroRadioButton dayRadioButton;
        private MetroFramework.Controls.MetroComboBox purposeComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private FontAwesome.Sharp.IconPictureBox exitButton;
        private FontAwesome.Sharp.IconPictureBox errorIconPictureBox;
        private System.Windows.Forms.Label errorlabel;
    }
}