﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CmmunityManagementSysWinFormsAppV2.WinForm
{
    public partial class Income : Form
    {
        public Income()
        {
            InitializeComponent();
        }

        private void viewIncomeButton_Click(object sender, EventArgs e)
        {
            incomeGrid.Rows.Clear();
            var start = fromDateTime.Value.Date;
            var end = toDateTime.Value.Date;
          
            using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
            {
                var query = (from n in dBEntitiesV2.Bookings
                             orderby n.BookingDate, n.Status, n.Status,n.Time
                             where n.Status == "Pending"
                             select n).Where(n => n.BookingDate >= start &&
                            n.BookingDate <= end).ToList();
                
                foreach (var i in query)
                {
                    if(i.DueAmount==0)
                    {
                       
                            int n = incomeGrid.Rows.Add();

                            incomeGrid.Rows[n].Cells[0].Value = i.BookingDate.Date;
                            incomeGrid.Rows[n].Cells[1].Value = i.Time;
                            incomeGrid.Rows[n].Cells[2].Value = i.Purpose;
                            incomeGrid.Rows[n].Cells[3].Value = i.TotalAmount;


                            incomeGrid.Rows[n].Cells[4].Value = i.PaidAmount;

                            incomeGrid.Rows[n].Cells[5].Value ="No Due";


                        }
                    else
                    {
                        int n = incomeGrid.Rows.Add();

                        incomeGrid.Rows[n].Cells[0].Value = i.BookingDate.Date;
                        incomeGrid.Rows[n].Cells[1].Value = i.Time;
                        incomeGrid.Rows[n].Cells[2].Value = i.Purpose;
                        incomeGrid.Rows[n].Cells[3].Value = i.TotalAmount;


                        incomeGrid.Rows[n].Cells[4].Value = i.PaidAmount;

                        incomeGrid.Rows[n].Cells[5].Value = i.DueAmount;


                    }



                }

           
                 
                        }
                       

                    }
                    
                   

                }
            }
        

