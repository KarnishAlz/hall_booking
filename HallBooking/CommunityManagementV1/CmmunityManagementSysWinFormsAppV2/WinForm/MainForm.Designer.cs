﻿namespace CmmunityManagementSysWinFormsAppV2.WinForm
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.leftPanel = new System.Windows.Forms.Panel();
            this.iconButton6 = new FontAwesome.Sharp.IconButton();
            this.generatePdfButton = new FontAwesome.Sharp.IconButton();
            this.logoutButton = new FontAwesome.Sharp.IconButton();
            this.addManagerButton = new FontAwesome.Sharp.IconButton();
            this.incomeButton = new FontAwesome.Sharp.IconButton();
            this.expenseButton = new FontAwesome.Sharp.IconButton();
            this.addBookingButton = new FontAwesome.Sharp.IconButton();
            this.calanderButton = new FontAwesome.Sharp.IconButton();
            this.logoPanel = new System.Windows.Forms.Panel();
            this.emaillabel = new System.Windows.Forms.Label();
            this.namelabel = new System.Windows.Forms.Label();
            this.iconPictureBox1 = new FontAwesome.Sharp.IconPictureBox();
            this.logoLabel = new System.Windows.Forms.Label();
            this.topPanel = new System.Windows.Forms.Panel();
            this.expandButton = new FontAwesome.Sharp.IconPictureBox();
            this.exitButton = new FontAwesome.Sharp.IconPictureBox();
            this.userlabel = new System.Windows.Forms.Label();
            this.iconCurrentFormPictureBox = new FontAwesome.Sharp.IconPictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.settingspanel = new System.Windows.Forms.Panel();
            this.changeBackgrounfbutton = new System.Windows.Forms.Button();
            this.clearBackgroundButton = new System.Windows.Forms.Button();
            this.changePassButton = new System.Windows.Forms.Button();
            this.changeHallNamebutton = new System.Windows.Forms.Button();
            this.backgroundPictureBox = new System.Windows.Forms.PictureBox();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.label1 = new System.Windows.Forms.Label();
            this.leftPanel.SuspendLayout();
            this.logoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox1)).BeginInit();
            this.topPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expandButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconCurrentFormPictureBox)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.settingspanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.backgroundPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // leftPanel
            // 
            this.leftPanel.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.leftPanel.Controls.Add(this.iconButton6);
            this.leftPanel.Controls.Add(this.generatePdfButton);
            this.leftPanel.Controls.Add(this.logoutButton);
            this.leftPanel.Controls.Add(this.addManagerButton);
            this.leftPanel.Controls.Add(this.incomeButton);
            this.leftPanel.Controls.Add(this.expenseButton);
            this.leftPanel.Controls.Add(this.addBookingButton);
            this.leftPanel.Controls.Add(this.calanderButton);
            this.leftPanel.Controls.Add(this.logoPanel);
            resources.ApplyResources(this.leftPanel, "leftPanel");
            this.leftPanel.Name = "leftPanel";
            this.leftPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.leftPanel_MouseDown);
            // 
            // iconButton6
            // 
            this.iconButton6.BackColor = System.Drawing.SystemColors.WindowFrame;
            resources.ApplyResources(this.iconButton6, "iconButton6");
            this.iconButton6.FlatAppearance.BorderSize = 0;
            this.iconButton6.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton6.ForeColor = System.Drawing.Color.White;
            this.iconButton6.IconChar = FontAwesome.Sharp.IconChar.Tools;
            this.iconButton6.IconColor = System.Drawing.Color.White;
            this.iconButton6.IconSize = 32;
            this.iconButton6.Name = "iconButton6";
            this.iconButton6.Rotation = 0D;
            this.iconButton6.UseVisualStyleBackColor = false;
            this.iconButton6.Click += new System.EventHandler(this.iconButton6_Click);
            // 
            // generatePdfButton
            // 
            this.generatePdfButton.BackColor = System.Drawing.SystemColors.WindowFrame;
            resources.ApplyResources(this.generatePdfButton, "generatePdfButton");
            this.generatePdfButton.FlatAppearance.BorderSize = 0;
            this.generatePdfButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.generatePdfButton.ForeColor = System.Drawing.Color.White;
            this.generatePdfButton.IconChar = FontAwesome.Sharp.IconChar.Book;
            this.generatePdfButton.IconColor = System.Drawing.Color.White;
            this.generatePdfButton.IconSize = 32;
            this.generatePdfButton.Name = "generatePdfButton";
            this.generatePdfButton.Rotation = 0D;
            this.generatePdfButton.UseVisualStyleBackColor = false;
            this.generatePdfButton.Click += new System.EventHandler(this.generatePdfButton_Click);
            // 
            // logoutButton
            // 
            this.logoutButton.BackColor = System.Drawing.SystemColors.WindowFrame;
            resources.ApplyResources(this.logoutButton, "logoutButton");
            this.logoutButton.FlatAppearance.BorderSize = 0;
            this.logoutButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.logoutButton.ForeColor = System.Drawing.Color.White;
            this.logoutButton.IconChar = FontAwesome.Sharp.IconChar.SignOutAlt;
            this.logoutButton.IconColor = System.Drawing.Color.White;
            this.logoutButton.IconSize = 32;
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Rotation = 0D;
            this.logoutButton.UseVisualStyleBackColor = false;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // addManagerButton
            // 
            this.addManagerButton.BackColor = System.Drawing.SystemColors.WindowFrame;
            resources.ApplyResources(this.addManagerButton, "addManagerButton");
            this.addManagerButton.FlatAppearance.BorderSize = 0;
            this.addManagerButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.addManagerButton.ForeColor = System.Drawing.Color.White;
            this.addManagerButton.IconChar = FontAwesome.Sharp.IconChar.Portrait;
            this.addManagerButton.IconColor = System.Drawing.Color.White;
            this.addManagerButton.IconSize = 32;
            this.addManagerButton.Name = "addManagerButton";
            this.addManagerButton.Rotation = 0D;
            this.addManagerButton.UseVisualStyleBackColor = false;
            this.addManagerButton.Click += new System.EventHandler(this.iconButton5_Click);
            // 
            // incomeButton
            // 
            this.incomeButton.BackColor = System.Drawing.SystemColors.WindowFrame;
            resources.ApplyResources(this.incomeButton, "incomeButton");
            this.incomeButton.FlatAppearance.BorderSize = 0;
            this.incomeButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.incomeButton.ForeColor = System.Drawing.Color.White;
            this.incomeButton.IconChar = FontAwesome.Sharp.IconChar.CheckCircle;
            this.incomeButton.IconColor = System.Drawing.Color.White;
            this.incomeButton.IconSize = 32;
            this.incomeButton.Name = "incomeButton";
            this.incomeButton.Rotation = 0D;
            this.incomeButton.UseVisualStyleBackColor = false;
            this.incomeButton.Click += new System.EventHandler(this.iconButton4_Click);
            // 
            // expenseButton
            // 
            this.expenseButton.BackColor = System.Drawing.SystemColors.WindowFrame;
            resources.ApplyResources(this.expenseButton, "expenseButton");
            this.expenseButton.FlatAppearance.BorderSize = 0;
            this.expenseButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.expenseButton.ForeColor = System.Drawing.Color.White;
            this.expenseButton.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.expenseButton.IconColor = System.Drawing.Color.White;
            this.expenseButton.IconSize = 32;
            this.expenseButton.Name = "expenseButton";
            this.expenseButton.Rotation = 0D;
            this.expenseButton.UseVisualStyleBackColor = false;
            this.expenseButton.Click += new System.EventHandler(this.iconButton3_Click);
            // 
            // addBookingButton
            // 
            this.addBookingButton.BackColor = System.Drawing.SystemColors.WindowFrame;
            resources.ApplyResources(this.addBookingButton, "addBookingButton");
            this.addBookingButton.FlatAppearance.BorderSize = 0;
            this.addBookingButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.addBookingButton.ForeColor = System.Drawing.Color.White;
            this.addBookingButton.IconChar = FontAwesome.Sharp.IconChar.PlusCircle;
            this.addBookingButton.IconColor = System.Drawing.Color.White;
            this.addBookingButton.IconSize = 32;
            this.addBookingButton.Name = "addBookingButton";
            this.addBookingButton.Rotation = 0D;
            this.addBookingButton.UseVisualStyleBackColor = false;
            this.addBookingButton.Click += new System.EventHandler(this.iconButton2_Click);
            // 
            // calanderButton
            // 
            this.calanderButton.BackColor = System.Drawing.SystemColors.WindowFrame;
            resources.ApplyResources(this.calanderButton, "calanderButton");
            this.calanderButton.FlatAppearance.BorderSize = 0;
            this.calanderButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.calanderButton.ForeColor = System.Drawing.Color.White;
            this.calanderButton.IconChar = FontAwesome.Sharp.IconChar.CalendarCheck;
            this.calanderButton.IconColor = System.Drawing.Color.White;
            this.calanderButton.IconSize = 32;
            this.calanderButton.Name = "calanderButton";
            this.calanderButton.Rotation = 0D;
            this.calanderButton.UseVisualStyleBackColor = false;
            this.calanderButton.Click += new System.EventHandler(this.iconButton1_Click);
            // 
            // logoPanel
            // 
            this.logoPanel.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.logoPanel.Controls.Add(this.emaillabel);
            this.logoPanel.Controls.Add(this.namelabel);
            this.logoPanel.Controls.Add(this.iconPictureBox1);
            resources.ApplyResources(this.logoPanel, "logoPanel");
            this.logoPanel.Name = "logoPanel";
            this.logoPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.logoPanel_MouseDown);
            // 
            // emaillabel
            // 
            resources.ApplyResources(this.emaillabel, "emaillabel");
            this.emaillabel.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.emaillabel.Name = "emaillabel";
            // 
            // namelabel
            // 
            resources.ApplyResources(this.namelabel, "namelabel");
            this.namelabel.ForeColor = System.Drawing.Color.BlanchedAlmond;
            this.namelabel.Name = "namelabel";
            // 
            // iconPictureBox1
            // 
            this.iconPictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.iconPictureBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.iconPictureBox1.IconChar = FontAwesome.Sharp.IconChar.User;
            this.iconPictureBox1.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.iconPictureBox1.IconSize = 43;
            resources.ApplyResources(this.iconPictureBox1, "iconPictureBox1");
            this.iconPictureBox1.Name = "iconPictureBox1";
            this.iconPictureBox1.TabStop = false;
            this.iconPictureBox1.Click += new System.EventHandler(this.iconPictureBox1_Click_1);
            // 
            // logoLabel
            // 
            resources.ApplyResources(this.logoLabel, "logoLabel");
            this.logoLabel.ForeColor = System.Drawing.Color.White;
            this.logoLabel.Name = "logoLabel";
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.topPanel.Controls.Add(this.logoLabel);
            this.topPanel.Controls.Add(this.expandButton);
            this.topPanel.Controls.Add(this.exitButton);
            this.topPanel.Controls.Add(this.userlabel);
            this.topPanel.Controls.Add(this.iconCurrentFormPictureBox);
            resources.ApplyResources(this.topPanel, "topPanel");
            this.topPanel.Name = "topPanel";
            this.topPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.topPanel_MouseDown);
            // 
            // expandButton
            // 
            this.expandButton.BackColor = System.Drawing.Color.Transparent;
            this.expandButton.IconChar = FontAwesome.Sharp.IconChar.WindowMinimize;
            this.expandButton.IconColor = System.Drawing.Color.White;
            this.expandButton.IconSize = 30;
            resources.ApplyResources(this.expandButton, "expandButton");
            this.expandButton.Name = "expandButton";
            this.expandButton.TabStop = false;
            this.expandButton.Click += new System.EventHandler(this.expandButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.Transparent;
            this.exitButton.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.exitButton.IconColor = System.Drawing.Color.White;
            this.exitButton.IconSize = 30;
            resources.ApplyResources(this.exitButton, "exitButton");
            this.exitButton.Name = "exitButton";
            this.exitButton.TabStop = false;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // userlabel
            // 
            resources.ApplyResources(this.userlabel, "userlabel");
            this.userlabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.userlabel.Name = "userlabel";
            // 
            // iconCurrentFormPictureBox
            // 
            this.iconCurrentFormPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.iconCurrentFormPictureBox.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.iconCurrentFormPictureBox.IconColor = System.Drawing.Color.White;
            this.iconCurrentFormPictureBox.IconSize = 34;
            resources.ApplyResources(this.iconCurrentFormPictureBox, "iconCurrentFormPictureBox");
            this.iconCurrentFormPictureBox.Name = "iconCurrentFormPictureBox";
            this.iconCurrentFormPictureBox.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.label1);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.settingspanel);
            this.panel1.Controls.Add(this.backgroundPictureBox);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.topPanel);
            this.panel1.Controls.Add(this.leftPanel);
            this.panel1.Controls.Add(this.shapeContainer1);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // settingspanel
            // 
            this.settingspanel.Controls.Add(this.changeBackgrounfbutton);
            this.settingspanel.Controls.Add(this.clearBackgroundButton);
            this.settingspanel.Controls.Add(this.changePassButton);
            this.settingspanel.Controls.Add(this.changeHallNamebutton);
            resources.ApplyResources(this.settingspanel, "settingspanel");
            this.settingspanel.Name = "settingspanel";
            // 
            // changeBackgrounfbutton
            // 
            this.changeBackgrounfbutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.changeBackgrounfbutton.FlatAppearance.BorderSize = 0;
            this.changeBackgrounfbutton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.changeBackgrounfbutton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            resources.ApplyResources(this.changeBackgrounfbutton, "changeBackgrounfbutton");
            this.changeBackgrounfbutton.ForeColor = System.Drawing.Color.LightGray;
            this.changeBackgrounfbutton.Name = "changeBackgrounfbutton";
            this.changeBackgrounfbutton.UseVisualStyleBackColor = false;
            this.changeBackgrounfbutton.Click += new System.EventHandler(this.changeBackgrounfbutton_Click);
            // 
            // clearBackgroundButton
            // 
            this.clearBackgroundButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.clearBackgroundButton.FlatAppearance.BorderSize = 0;
            this.clearBackgroundButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.clearBackgroundButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            resources.ApplyResources(this.clearBackgroundButton, "clearBackgroundButton");
            this.clearBackgroundButton.ForeColor = System.Drawing.Color.LightGray;
            this.clearBackgroundButton.Name = "clearBackgroundButton";
            this.clearBackgroundButton.UseVisualStyleBackColor = false;
            this.clearBackgroundButton.Click += new System.EventHandler(this.clearBackgroundButton_Click);
            // 
            // changePassButton
            // 
            this.changePassButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.changePassButton.FlatAppearance.BorderSize = 0;
            this.changePassButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.changePassButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            resources.ApplyResources(this.changePassButton, "changePassButton");
            this.changePassButton.ForeColor = System.Drawing.Color.LightGray;
            this.changePassButton.Name = "changePassButton";
            this.changePassButton.UseVisualStyleBackColor = false;
            this.changePassButton.Click += new System.EventHandler(this.changePassButton_Click);
            // 
            // changeHallNamebutton
            // 
            this.changeHallNamebutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.changeHallNamebutton.FlatAppearance.BorderSize = 0;
            this.changeHallNamebutton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.changeHallNamebutton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            resources.ApplyResources(this.changeHallNamebutton, "changeHallNamebutton");
            this.changeHallNamebutton.ForeColor = System.Drawing.Color.LightGray;
            this.changeHallNamebutton.Name = "changeHallNamebutton";
            this.changeHallNamebutton.UseVisualStyleBackColor = false;
            this.changeHallNamebutton.Click += new System.EventHandler(this.changeHallNamebutton_Click);
            // 
            // backgroundPictureBox
            // 
            resources.ApplyResources(this.backgroundPictureBox, "backgroundPictureBox");
            this.backgroundPictureBox.Name = "backgroundPictureBox";
            this.backgroundPictureBox.TabStop = false;
            // 
            // shapeContainer1
            // 
            resources.ApplyResources(this.shapeContainer1, "shapeContainer1");
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            resources.ApplyResources(this.lineShape2, "lineShape2");
            this.lineShape2.Name = "lineShape2";
            // 
            // lineShape1
            // 
            resources.ApplyResources(this.lineShape1, "lineShape1");
            this.lineShape1.Name = "lineShape1";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label1.Name = "label1";
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
            this.leftPanel.ResumeLayout(false);
            this.logoPanel.ResumeLayout(false);
            this.logoPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconPictureBox1)).EndInit();
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expandButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.exitButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconCurrentFormPictureBox)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.settingspanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.backgroundPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel leftPanel;
        private FontAwesome.Sharp.IconButton logoutButton;
        private FontAwesome.Sharp.IconButton addManagerButton;
        private FontAwesome.Sharp.IconButton incomeButton;
        private FontAwesome.Sharp.IconButton expenseButton;
        private FontAwesome.Sharp.IconButton addBookingButton;
        private FontAwesome.Sharp.IconButton calanderButton;
        private System.Windows.Forms.Panel logoPanel;
        private System.Windows.Forms.Label logoLabel;
        private System.Windows.Forms.Panel topPanel;
        private FontAwesome.Sharp.IconPictureBox expandButton;
        private FontAwesome.Sharp.IconPictureBox exitButton;
        private System.Windows.Forms.Label userlabel;
        private FontAwesome.Sharp.IconPictureBox iconCurrentFormPictureBox;
        private FontAwesome.Sharp.IconPictureBox iconPictureBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private FontAwesome.Sharp.IconButton generatePdfButton;
        private System.Windows.Forms.PictureBox backgroundPictureBox;
        private System.Windows.Forms.Label emaillabel;
        private System.Windows.Forms.Label namelabel;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Panel settingspanel;
        private System.Windows.Forms.Button changePassButton;
        private System.Windows.Forms.Button changeBackgrounfbutton;
        private System.Windows.Forms.Button changeHallNamebutton;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private System.Windows.Forms.Button clearBackgroundButton;
        private FontAwesome.Sharp.IconButton iconButton6;
        private System.Windows.Forms.Label label1;
    }
}