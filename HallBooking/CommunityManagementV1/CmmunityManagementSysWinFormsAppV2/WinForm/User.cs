﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CmmunityManagementSysWinFormsAppV2.WinForm
{
    public partial class User : Form
    {
        public User()
        {
            InitializeComponent();
        }

        private void adminButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdminLogin adminLogin = new AdminLogin();
            adminLogin.ShowDialog();


        }

        private void managerButton_Click(object sender, EventArgs e)
        {
            string user = "Manager";
            this.Hide();
            AdminLogin adminLogin = new AdminLogin(user);
            adminLogin.ShowDialog();
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void expandButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void User_Load(object sender, EventArgs e)
        {
            using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
            {
                var setting = dBEntitiesV2.Settings.Where(f => f.Id == 1).FirstOrDefault();
                if (setting != null)
                {
                    logoLabel.Text = setting.Name;

                }
                else
                {
                    logoLabel.Text = "";
                }
            }
        }
    }
}
