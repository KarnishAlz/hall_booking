﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using CmmunityManagementSysWinFormsAppV2.Cache;

namespace CmmunityManagementSysWinFormsAppV2.WinForm
{
    public partial class AdminLogin : Form
    {
        string _user;
        public AdminLogin()

        {
            InitializeComponent();
           
        }
        public AdminLogin( string userId)

        {
           
            InitializeComponent();
            _user = userId;
            UserButton.Text = _user;


        }

        private void LoginButton_Click(object sender, EventArgs e)
        {

            using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
            {
               
                if(_user!="Manager")
                {
                    Admin result = (from n in dBEntitiesV2.Admins
                                    orderby n.Admin1, n.Password,n.Id

                                    select n).FirstOrDefault();
                    if (userTextBox.Text != "UserName")
                    {
                        if (passwordTextBox.Text != "Password")
                        {
                            if (userTextBox.Text.Trim() == result.Admin1)
                            {
                                if (passwordTextBox.Text.Trim() == result.Password)
                                {
                                    UserLoginCache.Admin = result.Admin1;
                                    UserLoginCache.Password = result.Password;


                                    string manager = "User";
                                    MainForm mainForm = new MainForm(manager);
                                    this.Close();
                                    MessageBox.Show("Welcome " + UserLoginCache.Admin);
                                    mainForm.Show();
                                    mainForm.FormClosed += logOut;



                                }
                                else
                                {
                                    errorMessageShow("Your Information Is incorrect");
                                    passwordTextBox.Text = "Password";
                                    passwordTextBox.UseSystemPasswordChar = false;
                                    userTextBox.Focus();
                                }

                            }
                            else
                            {
                                errorMessageShow("Your Information Is incorrect");

                            }

                        }
                        else
                        {
                            errorMessageShow("Please Enter Password");

                        }

                    }
                    else
                    {
                        errorMessageShow("Please Enter UserName");
                    }



                }
                else
                {
                    var result = (from n in dBEntitiesV2.ManagerCreates
                                            orderby n.Email, n.Password,n.Name
                                            where n.Email== userTextBox.Text.Trim()
                                            select n).First();
                    if (userTextBox.Text != "UserName")
                    {
                        if (passwordTextBox.Text != "Password")
                        {
                           
                            if (userTextBox.Text.Trim() == result.Email)
                            {
                                if (passwordTextBox.Text.Trim() == result.Password)
                                {
                                    managerLogin.Email = result.Email;
                                    managerLogin.Name = result.Name;

                                    string manager = "Manager";
                                    MainForm mainForm = new MainForm(manager);
                                    this.Close();
                                    MessageBox.Show("Welcome " + managerLogin.Name);
                                    mainForm.Show();
                                    mainForm.FormClosed += logOut;



                                }
                                else
                                {
                                    errorMessageShow("Your Information Is incorrect");
                                    passwordTextBox.Text = "Password";
                                    passwordTextBox.UseSystemPasswordChar = false;
                                    userTextBox.Focus();
                                }

                            }
                            else
                            {
                                errorMessageShow("Your Information Is incorrect");

                            }

                        }
                        else
                        {
                            errorMessageShow("Please Enter Password");

                        }

                    }
                    else
                    {
                        errorMessageShow("Please Enter UserName");
                    }


                }





            }
        }


            
       

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void expandButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void userTextBox_Enter(object sender, EventArgs e)
        {
            if (userTextBox.Text == "UserName")
            {
                userTextBox.Text = "";
                userTextBox.ForeColor = Color.LightGray;
            }
        }

        private void passwordTextBox_Enter(object sender, EventArgs e)
        {
            if (passwordTextBox.Text == "Password")
            {
                passwordTextBox.Text = "";
                passwordTextBox.ForeColor = Color.LightGray;
                passwordTextBox.UseSystemPasswordChar = true;
            }

        }

        private void userTextBox_Leave(object sender, EventArgs e)
        {
            if (userTextBox.Text == "")
            {
                userTextBox.Text = "UserName";
                userTextBox.ForeColor = Color.DimGray;
            }
        }

        private void passwordTextBox_Leave(object sender, EventArgs e)
        {
            if (passwordTextBox.Text == "")
            {
                passwordTextBox.Text = "Password";
                passwordTextBox.ForeColor = Color.DimGray;
                passwordTextBox.UseSystemPasswordChar = false;
            }

        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        private void AdminLogin_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);

        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);

        }
        private void errorMessageShow(string message)
        {
            errorlabel.Text = "   " + message;
            errorlabel.Visible = true;
            errorIconPictureBox.Visible = true;
        }
        private void logOut(object sender,FormClosedEventArgs e)
        {
            User user = new User();
            user.Show();

        }

       
    }
}
