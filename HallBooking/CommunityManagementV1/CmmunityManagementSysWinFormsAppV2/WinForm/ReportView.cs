﻿using CmmunityManagementSysWinFormsAppV2.Cache;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CmmunityManagementSysWinFormsAppV2.WinForm
{
    public partial class ReportView : Form
    {
        
       
        List<BookingViewReport> _bookingViewReportList = new List<BookingViewReport>();

        ViewBooking viewBookingform;
        public ReportView(ViewBooking form)
        {

            InitializeComponent();
            viewBookingform = form;
           
         
        }

        private void ReportView_Load(object sender, EventArgs e)
        {
            BookingReport bookingReport = new BookingReport();

            
            
            int n = viewBookingform.dataGridView.RowCount;
            if (n > 0)
            {
                for (int i = 0; i < n; i++)
                {
                    BookingViewReport bookingViewReport = new BookingViewReport();
                    bookingViewReport.Date = viewBookingform.dataGridView.Rows[i].Cells[1].Value.ToString();
                    bookingViewReport.Purpose = viewBookingform.dataGridView.Rows[i].Cells[2].Value.ToString();
                    bookingViewReport.CustomerName = viewBookingform.dataGridView.Rows[i].Cells[3].Value.ToString();
                    bookingViewReport.CustomerPhone = viewBookingform.dataGridView.Rows[i].Cells[4].Value.ToString();
                    bookingViewReport.TotalAmount = Convert.ToInt32(viewBookingform.dataGridView.Rows[i].Cells[5].Value);
                    bookingViewReport.PaidAmount = Convert.ToInt32(viewBookingform.dataGridView.Rows[i].Cells[6].Value);
                    bookingViewReport.DueAmount = Convert.ToInt32(viewBookingform.dataGridView.Rows[i].Cells[7].Value);

                    _bookingViewReportList.Add(bookingViewReport);

                }
          
                    bookingReport.SetDataSource(_bookingViewReportList);
                    bookingReport.SetParameterValue("start", viewBookingform.fromDateTime.Value.ToString());
                    bookingReport.SetParameterValue("end", viewBookingform.toDateTime.Value.ToString());

                    crystalReportViewer1.ReportSource = bookingReport;
                    crystalReportViewer1.Refresh();
                }

            
            else
            {

                MessageBox.Show("Incorrect Datetime");
            }
            
        }
      

    }
}
