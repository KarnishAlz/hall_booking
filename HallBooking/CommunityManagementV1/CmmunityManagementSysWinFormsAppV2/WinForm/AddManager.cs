﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CmmunityManagementSysWinFormsAppV2.WinForm
{
    public partial class AddManager : Form
    {
        public AddManager()
        {
            InitializeComponent();
            updateGridManager();
        }
        private void createButton_Click(object sender, EventArgs e)
        {
            if (isFieldNull() != false)
            {
                string _email = emailTextBox.Text.Trim();
                bool isexist = ManagerExists(_email);
                if (isexist != true)
                {
                    saveManager();
                    updateGridManager();
                    nameTextBox.Clear();
                    emailTextBox.Clear();
                    positionRichTextBox.Clear();
                }
            }
            else
            {
                errorIconPictureBox.Visible = true;
                errorlabel.Visible = true;
                errorlabel.Text = "Please Provide All The Information";

            }
           

            


        }
        public void updateGridManager()
        {
            dataGridView.Rows.Clear();


            using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
            {
                var query = (from n in dBEntitiesV2.ManagerCreates
                             orderby n.Name, n.Email, n.JoiningDate,n.Position

                             select n).ToList();
                if (query.Count != 0)
                {
                    foreach (var i in query)
                    {


                        int n = dataGridView.Rows.Add();
                        foreach (var j in query)
                        {
                            dataGridView.Rows[n].Cells[0].Value = i.Name;
                            dataGridView.Rows[n].Cells[1].Value = i.Email;
                            dataGridView.Rows[n].Cells[2].Value = i.JoiningDate.Date;
                            dataGridView.Rows[n].Cells[3].Value = i.Position;



                        }

                    }


                }
            }
        }

        public bool ManagerExists(string email)
        {
            using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
            {
                var result = dBEntitiesV2.ManagerCreates.Where(f => f.Email== email).FirstOrDefault();
                if (result != null)
                {
                    return true;
                }
                else
                    return false;


            }

        }
        public void saveManager()
        {
            string query = "INSERT INTO ManagerCreate (Name, Position, Email,Password,JoiningDate) VALUES(@name, @position, @email,@pass,@join)";
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-6KNP71K;Initial Catalog=CommunityManagementDB;Integrated Security=True");
            SqlCommand cmd = new SqlCommand(query, con);

            cmd.Parameters.AddWithValue("@name", nameTextBox.Text);
            cmd.Parameters.AddWithValue("@position", positionRichTextBox.Text);
            cmd.Parameters.AddWithValue("@email", emailTextBox.Text);
            cmd.Parameters.AddWithValue("@pass", nameTextBox.Text);
            cmd.Parameters.AddWithValue("@join", DateTime.Today);

            con.Open();
            int i = cmd.ExecuteNonQuery();

            con.Close();

            if (i != 0)
            {
                MessageBox.Show(i + "Data Saved");
                nameTextBox.Clear();
                positionRichTextBox.Clear();
                emailTextBox.Clear();
            }

           

        }

        private void AddManager_Load(object sender, EventArgs e)
        {
            updateGridManager();
        }
        public bool isFieldNull()


        {
            if (nameTextBox.Text != "")
            {
               if(emailTextBox.Text!="")
                {
                    return true;

                }
               else
                {
                    return false;
                }
         
        
            }
            else
            {

                return false;
            }


        }
    }

}
