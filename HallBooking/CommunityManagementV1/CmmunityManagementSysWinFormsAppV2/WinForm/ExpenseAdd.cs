﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CmmunityManagementSysWinFormsAppV2.WinForm
{
    public partial class ExpenseAdd : Form
    {
        public ExpenseAdd()
        {
            InitializeComponent();
        }
        Expense expense = new Expense();
        string time = "";
        private void ExpenseAdd_Load(object sender, EventArgs e)
        {
            expenseMetroTabControl.BringToFront();

        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            
            using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
            {
                if (isFieldNull() != false)
                {
                    expense = GetValuesFromForm();
                    dBEntitiesV2.Expenses.Add(expense);
                    dBEntitiesV2.SaveChanges();
                    MessageBox.Show("saved");
                    clear();
                }
                else
                {
                    errorIconPictureBox.Visible = true;
                    errorlabel.Visible = true;
                    errorlabel.Text = "Please Provide All The Information";

                }

             
            }
        }
        private void viewButton_Click(object sender, EventArgs e)
        {
           expenseGrid.Rows.Clear();
            var start = fromDateTime.Value.Date;
            var end = toDateTime.Value.Date;

            using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
            {
               
                    var result = (from x in dBEntitiesV2.Expenses
                                  orderby x.Amount, x.Description,x.Date,x.Title
                                  select x).ToList();
                    if(result.Count!=0)
                    {
                        int n = expenseGrid.Rows.Add();
                        foreach (var i in result)
                        {
                            expenseGrid.Rows[n].Cells[0].Value = i.Date;
                            expenseGrid.Rows[n].Cells[1].Value = i.Title;
                            expenseGrid.Rows[n].Cells[2].Value = i.Description;
                            expenseGrid.Rows[n].Cells[3].Value = i.Amount;
                           

                        }

                    }
                   


                }
            
        }

        public Expense GetValuesFromForm()
        {


            expense.Date = dateDateTime.Value;
            expense.Description = descriptionTextBox.Text.Trim();
            expense.Amount = Convert.ToInt32(amountTextBox.Text.Trim());
            expense.Title = timeComboBox.SelectedItem.ToString();

            return expense;

        }


        public bool isFieldNull()


        {
            if (timeComboBox.SelectedItem != null)
            {
                if (amountTextBox.Text != "")
                {
                    return true;
                }
                else
                {

                    return false;
                }
            }
            else
            {

                return false;
            }


        }
        public void clear()
        {
            timeComboBox.SelectedItem = null;
            descriptionTextBox.Clear();
            amountTextBox.Clear();

        }



    }



    }

    

