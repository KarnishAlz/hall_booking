﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CmmunityManagementSysWinFormsAppV2.WinForm
{
    public partial class BookingAdd : Form
    {
        Booking booking = new Booking();
        string time = "";
        public int bookingId; 
        bool isExist;
        public BookingAdd()
        {
            InitializeComponent();
            bookingId = 0;
            dateTimePicker1.Value = DateTime.Today;
            exitButton.Visible = false;
        }

        public BookingAdd(DateTime date)
        {
            InitializeComponent();
            bookingId = 0;
            dateTimePicker1.Value = date;
            clear();
        }

        public BookingAdd(Booking booking)
        {

            InitializeComponent();
            bookingId = booking.Id;
            ShowBookingDetails(booking);

        }
        Booking Booking = new Booking();
        private void ShowBookingDetails(Booking _booking)
        {
            bookingId = _booking.Id;
            purposeComboBox.Text = _booking.Purpose;
            if (_booking.Time == "Day")
            {
                dayRadioButton.Checked = true;
                nightRadioButton.Enabled = false;
            }
            else
            nightRadioButton.Checked = true;
            dayRadioButton.Enabled = false;
            dateTimePicker1.Value = _booking.BookingDate;
            dateTimePicker1.Enabled = false;
            totalTextBox.Text = _booking.TotalAmount.ToString();
            paidTextBox.Text = _booking.PaidAmount.ToString();
            dueTextBox.Text = _booking.DueAmount.ToString();
            customerNameTextBox.Text = _booking.CustomerName.ToString();
            customerPhoneTextBox.Text = _booking.CustomerPhone.ToString();
            if (customerMailTextBox.Text != null)
            {
                customerMailTextBox.Text = _booking.CustomerEmail;
            }
            else
                customerMailTextBox.Text = "";

        }

       
        private void Booking_Load(object sender, EventArgs e)
        {
            //clear();
            
            if(bookingId==0)
            {
                deleteButton.Visible = false;
                
            }
            
            
        }
        public void clear()
        {
            purposeComboBox.Text = "--select--";
            purposeComboBox.SelectedItem = null;
            totalTextBox.Clear();
            paidTextBox.Clear();
            dueTextBox.Clear();
            customerMailTextBox.Clear();
            customerNameTextBox.Clear();
            customerPhoneTextBox.Clear();
            nightRadioButton.Checked = false;
            dayRadioButton.Checked = false;

        }



        private void saveButton_Click(object sender, EventArgs e)
        {
            using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
            {
                Booking _booking = new Booking();
               
               
                if (bookingId == 0)
                {
                   
                    if (isFieldNull() != false)
                    {
                        _booking = GetValuesFromForm();
                        isExist = BookingExit(_booking.BookingDate, _booking.Time);

                    }
                   
                    
                }
                else
                {
                   
                        _booking = GetValuesFromForm();
                    if (isFieldNull() != false)
                    {
                       
                            var user = dBEntitiesV2.Bookings.Where(x=> x.Id==bookingId).First();
                            user.CustomerName = _booking.CustomerName;
                        user.CustomerEmail = _booking.CustomerEmail;
                        user.CustomerPhone = _booking.CustomerPhone;
                        user.Purpose = _booking.Purpose;
                        user.TotalAmount = _booking.TotalAmount;
                        user.PaidAmount = _booking.PaidAmount;
                        user.DueAmount = _booking.DueAmount;
                        dBEntitiesV2.SaveChanges();
                        MessageBox.Show("saved");
                        this.Close();
                    }

                    isExist = true;
                }
                
                if (isExist != true)
                {
                    if(isFieldNull()!=false)
                    {
                        dBEntitiesV2.Bookings.Add(_booking);
                        dBEntitiesV2.SaveChanges();
                        MessageBox.Show("saved");
                        this.Close();
                    }
                    else
                    {
                        errorIconPictureBox.Visible = true;
                        errorlabel.Visible = true;
                        errorlabel.Text = "Please Provide All The Information";

                    }

                }
               else
                {
                    errorlabel.Visible = true;
                    errorIconPictureBox.Visible = true;
                    errorlabel.Text = "This Date & Time is Already Booked";
                }
                
               


            }
        }
        public Booking GetValuesFromForm()
        {

            if (dayRadioButton.Checked)
            {
                time = dayRadioButton.Text;
            }

            if (nightRadioButton.Checked)
            {
                time = nightRadioButton.Text;
            }
            booking.CustomerEmail = customerMailTextBox.Text.Trim();
            booking.CustomerName = customerNameTextBox.Text.Trim();
            booking.CustomerPhone = customerPhoneTextBox.Text.Trim();
            booking.BookingDate = dateTimePicker1.Value;
            booking.Time = time;
            booking.Purpose = purposeComboBox.SelectedItem.ToString();
            booking.TotalAmount = Convert.ToInt32(totalTextBox.Text.Trim());
            booking.PaidAmount = Convert.ToInt32(paidTextBox.Text.Trim());
            booking.DueAmount = Convert.ToInt32(dueTextBox.Text.Trim());
            booking.Status = "Pending";
            booking.Id = bookingId;
           
            return booking;

        }
        private void paidTextBox_TextChanged(object sender, EventArgs e)
        {
            if (totalTextBox.Text != "")
            {
                if(paidTextBox.Text!="")
                {
                    int total = Convert.ToInt32(totalTextBox.Text);
                    int paid = Convert.ToInt32(paidTextBox.Text);
                    int due = total - paid;
                    if(due<0)
                    {
                        dueTextBox.Text = "0";

                    }
                    else
                    {
                        dueTextBox.Text = due.ToString();

                    }



                }

            }
            else
            {
                dueTextBox.Text = "";
            }

        }


        private void totalTextBox_TextChanged(object sender, EventArgs e)
        {
            if (paidTextBox.Text != "")
            {
                int total = Convert.ToInt32(totalTextBox.Text);
                int paid = Convert.ToInt32(paidTextBox.Text);
                int due = total - paid;
                dueTextBox.Text = due.ToString();



            }

        }

      
        private void deleteButton_Click(object sender, EventArgs e)
        {
            using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
            {
                Booking _booking = new Booking();

                if (bookingId != 0)
                {
                    _booking = GetValuesFromForm();
                    var result = (from p in dBEntitiesV2.Bookings
                                  where p.Id == bookingId
                                  select p);

                    foreach (var booking in result)
                    {
                        booking.Status = "Canceled";
                    }
                    dBEntitiesV2.SaveChanges();
                    MessageBox.Show("Deleted");
                    this.Close();
                }



            }

        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
            
        }

        public bool BookingExit(DateTime dateTime,string time )
        {
            using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
            {
                var result = dBEntitiesV2.Bookings.Where(f => f.BookingDate == dateTime && f.Time== time).FirstOrDefault();
                if (result != null)
                {
                    return true;
                }
                else
                    return false;


            }

        }

        public  bool isFieldNull()
        
        
        {
            if(purposeComboBox.SelectedItem!=null )
            {
                if (totalTextBox.Text!= "")
                {
                    if (paidTextBox.Text!="")
                    {
                        if (customerNameTextBox.Text!= "")
                        {
                            if (customerPhoneTextBox.Text!= "")
                            {
                                return true;
                            }
                            else
                            {
                                
                                return false;
                            }
                        }
                        else
                        {
                           
                            return false;
                        }
                    }
                    else
                    {
                        
                        return false;

                    }
                }
                else
                {
                    
                    return false;

                }
            }
           
            else
            {
                return false;

            }
               
           
           
        }

     
    }
}
    

