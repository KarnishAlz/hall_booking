﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using PagedList;
using CmmunityManagementSysWinFormsAppV2.Cache;

namespace CmmunityManagementSysWinFormsAppV2.WinForm
{
    public partial class ViewBooking : Form
    {
        public ViewBooking()
        {
            InitializeComponent();
        }
      
        List<BookingViewReport> bookingViewReports = new List<BookingViewReport>();
        private void viewButton_Click(object sender, EventArgs e)
        {
            dataGridView.Rows.Clear();
            BookingViewReport bookingViewReport = new BookingViewReport();

            var start = fromDateTime.Value.Date;

            var end = DateTime.Today.Date;

            using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
            {
                var query = (from n in dBEntitiesV2.Bookings
                             orderby n.BookingDate, n.Status, n.Time
                             where n.Status == "Pending"
                             select n).Where(n => n.BookingDate >= start &&
                            n.BookingDate <= end).ToList();
                if (query.Count != 0)
                {
                    foreach (var i in query)
                    {


                        int n = dataGridView.Rows.Add();
                        foreach (var j in query)
                        {
                            dataGridView.Rows[n].Cells[0].Value = i.BookingDate.Date + "," + i.Time;
                            dataGridView.Rows[n].Cells[1].Value = i.Purpose;


                            dataGridView.Rows[n].Cells[3].Value = i.CustomerName;
                            dataGridView.Rows[n].Cells[4].Value = i.CustomerPhone;

                            dataGridView.Rows[n].Cells[5].Value = i.TotalAmount;
                            dataGridView.Rows[n].Cells[6].Value = i.PaidAmount;
                            dataGridView.Rows[n].Cells[7].Value = i.DueAmount;

                            if (i.BookingDate.Date <= DateTime.Today)
                            {
                                dataGridView.Rows[n].Cells[2].Value = "Complete";

                            }
                            else
                            {
                                dataGridView.Rows[n].Cells[2].Value = i.Status;
                            }


                        }

                    }



                }

            }




        }

       
        private void prinrtButton_Click(object sender, EventArgs e)
        {
           
           
            ReportView reportView = new ReportView(this);
            reportView.Show();


        }

        
    }
}

    
