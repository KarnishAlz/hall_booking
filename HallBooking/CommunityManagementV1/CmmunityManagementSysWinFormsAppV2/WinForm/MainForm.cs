﻿using FontAwesome.Sharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CmmunityManagementSysWinFormsAppV2.Cache;
using System.IO;

namespace CmmunityManagementSysWinFormsAppV2.WinForm
{
    public partial class MainForm : Form
    {
        private IconButton currentButton;
        private Panel leftBorderButton;
        private Form currentChildFrom;
        string _user = "";
        public MainForm(string user)
        {
            _user = user;
            InitializeComponent();
            leftBorderButton = new Panel();
            leftBorderButton.Size = new Size(7, 60);
            leftPanel.Controls.Add(leftBorderButton);
            
            this.Text = string.Empty;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            
            
        }
       
            
        

        private struct RGBColors
        {
            public static Color color1 = Color.Pink;
            public static Color color2 = Color.LightBlue;
            public static Color color3 = Color.FromArgb(255, 255, 128);
            public static Color color4 = Color.Thistle;
            public static Color color5 = Color.FromArgb(255, 128, 128);
            public static Color color6 = Color.FromArgb(128, 255, 128);
            public static Color color7 = Color.FromArgb(255, 224, 192);

        }
        private void ActivateButton(object sender, Color color)
        {
            DisableButton();
            if (sender != null)
            {
                currentButton = (IconButton)sender;
                currentButton.BackColor = SystemColors.WindowFrame;
                currentButton.ForeColor = color;
                currentButton.TextAlign = ContentAlignment.MiddleCenter;
                currentButton.IconColor = color;
                currentButton.TextImageRelation = TextImageRelation.TextBeforeImage;
                currentButton.ImageAlign = ContentAlignment.MiddleRight;
                leftBorderButton.BackColor = color;
                leftBorderButton.Location = new Point(0, currentButton.Location.Y);
                leftBorderButton.Visible = true;
                leftBorderButton.BringToFront();
                iconCurrentFormPictureBox.IconChar = currentButton.IconChar;
                iconCurrentFormPictureBox.IconColor = color;

            }
        }

        private void DisableButton()
        {
            if (currentButton != null)
            {
                currentButton.BackColor = SystemColors.WindowFrame;
                currentButton.ForeColor = Color.White;
                currentButton.TextAlign = ContentAlignment.MiddleLeft;
                currentButton.IconColor = Color.White;
                currentButton.TextImageRelation = TextImageRelation.ImageBeforeText;
                currentButton.ImageAlign = ContentAlignment.MiddleLeft;

            }
        }

        private void iconButton1_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, RGBColors.color6);
            openForm(new CalandarView());
        }

        private void iconButton2_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, RGBColors.color6);
            openForm(new BookingAdd());

        }

        private void iconButton3_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, RGBColors.color6);
            openForm(new ExpenseAdd());
        }

        private void iconButton4_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, RGBColors.color6);
            openForm(new Income());
        }

        private void iconButton5_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, RGBColors.color6);
            openForm(new AddManager());
        }

        private void iconButton6_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, RGBColors.color6);
            
           if(currentChildFrom == null)
            {
                
                settingspanel.Show();
            }
           else if(currentChildFrom.Visible)
            {
                currentChildFrom.Hide();
                settingspanel.Show();
            }
         
           else
            {
                settingspanel.Show();
            }



        }
        private void openForm(Form childrenForm)
        {
            settingspanel.Hide();
            if (currentChildFrom != null)
            {
                currentChildFrom.Close();
            }
            currentChildFrom= childrenForm;
            childrenForm.TopLevel = false;
            childrenForm.FormBorderStyle = FormBorderStyle.None;
            childrenForm.Dock = DockStyle.Fill;
            backgroundPictureBox.Controls.Add(childrenForm);
            backgroundPictureBox.Tag = childrenForm;
            childrenForm.BackgroundImageLayout = ImageLayout.Stretch;
            childrenForm.BackgroundImage = backgroundPictureBox.Image;
            childrenForm.BringToFront();
            childrenForm.Show();



        }

        private void homePictureBox_Click(object sender, EventArgs e)
        {
            Reset();
        }
        private void Reset()
        {
            DisableButton();
            leftBorderButton.Visible = false;
            iconCurrentFormPictureBox.IconChar = IconChar.Home;
            iconCurrentFormPictureBox.IconColor = Color.MediumTurquoise;
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You Sure To Close The Application?", "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                this.Close();
              
            }

            
        }

        private void LoadUserData()
        {
            if(_user=="Manager")
            {
                namelabel.Text = managerLogin.Name;
                emaillabel.Text = managerLogin.Email;
                userlabel.Text = _user;
                addManagerButton.Hide();
                incomeButton.Hide();
            }
            else
            {
                namelabel.Text = UserLoginCache.Admin;
                userlabel.Text = "Admin";
            }
            
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
           
            settingspanel.Hide();
            LoadUserData();
            using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
            {
                var setting = dBEntitiesV2.Settings.Where(f => f.Id == 1).FirstOrDefault();
                logoLabel.Text = setting.Name;
                if(setting.Image!=null)
                {
                    Image img = Image.FromFile(setting.Image);
                    backgroundPictureBox.Image = img;
                    backgroundPictureBox.ImageLocation = setting.Image;

                }
                else
                {
                    backgroundPictureBox.Image = null;
                }

                openForm(new CalandarView());

            }
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        private void MainForm_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void leftPanel_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void logoPanel_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);

        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void expandButton_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        
    }

        private void topPanel_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);


        }

        private void generatePdfButton_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, RGBColors.color6);
            openForm(new ViewBooking());
        }

        private void profileButton_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, RGBColors.color6);
            openForm(new ViewBooking());

        }

        private void changeBackgrounfbutton_Click(object sender, EventArgs e)
        {
            string imgelocation = "";
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "jpg file(*.jpg)|*.jpg|png file(*.png)|*.png|All files(*.*)|*.*";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                imgelocation = dialog.FileName;
                Image img = Image.FromFile(imgelocation);
                backgroundPictureBox.Image = img;
                backgroundPictureBox.ImageLocation = imgelocation;
                
                try
                {
                   
                    using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
                    {
                        var setting = dBEntitiesV2.Settings.Where(f => f.Id == 1).FirstOrDefault();
                        
                            setting.Image = imgelocation;
                            dBEntitiesV2.SaveChanges();

                        }

                    }
                catch 
                {
                    MessageBox.Show("Something Went Wrong");

                }

            }
        }
       




        private void changeHallNamebutton_Click(object sender, EventArgs e)
        {
            
            var text=ShowDialog(" ", "Please Enter A Name");
            logoLabel.Text = text;
            
                using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
                {
                    var setting = dBEntitiesV2.Settings.Where(f => f.Id == 1).FirstOrDefault();
                if (logoLabel.Text != "")
                {
                    setting.Name = text;
                    dBEntitiesV2.SaveChanges();

                }
                else
                {

                    setting.Name = "Hall Booking";
                    dBEntitiesV2.SaveChanges();

                }

            }
           
        }

            private void changePassButton_Click(object sender, EventArgs e)
        {
            var text = ShowDialog(" ", "Please Enter A Password");
            if(userlabel.Text!="Admin")
            {
                var usermail = emaillabel.Text.Trim();
                using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
                {
                    var managerCreate = dBEntitiesV2.ManagerCreates.Where(f => f.Email == usermail).FirstOrDefault();
                    if (text != "")
                    {
                        managerCreate.Password = text;
                        dBEntitiesV2.SaveChanges();


                    }


                }

            }
            else
            {
                var user = namelabel.Text.Trim();
                using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
                {
                    var managerCreate = dBEntitiesV2.Admins.Where(f => f.Id ==1 && f.Admin1==user).FirstOrDefault();
                    if (text != "")
                    {
                        managerCreate.Password = text;
                        dBEntitiesV2.SaveChanges();


                    }


                }

            }
           

            }







            public string ShowDialog(string text, string caption)
        {
            string logo="";
            Form prompt = new Form()
            {
                Width = 500,
                Height = 150,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = caption,
                StartPosition = FormStartPosition.CenterScreen
            };
            Label textLabel = new Label() { Left = 50, Top = 20, Text = text };
            TextBox textBox = new TextBox() { Left = 50, Top = 50, Width = 400 };
            Button confirmation = new Button() { Text = "Ok", Left = 350, Width = 100, Top = 70, DialogResult = DialogResult.OK };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            prompt.Controls.Add(textBox);
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.AcceptButton = confirmation;

            if (prompt.ShowDialog() == DialogResult.OK)
            {
                if(textBox.Text!=null)
                {
                    logo= textBox.Text.Trim();

                }
            
            }
            return logo;
        }

        private void clearBackgroundButton_Click(object sender, EventArgs e)
        {
            try
            {

                using (CommunityManagementDBEntities dBEntitiesV2 = new CommunityManagementDBEntities())
                {
                    var setting = dBEntitiesV2.Settings.Where(f => f.Id == 1).FirstOrDefault();

                    setting.Image = null;
                    dBEntitiesV2.SaveChanges();

                }
                backgroundPictureBox.Image = null;

            }
            catch
            {
                MessageBox.Show("Something Went Wrong");
            }
            
        }

      

        private void iconPictureBox1_Click_1(object sender, EventArgs e)
        {
            iconCurrentFormPictureBox.IconChar = IconChar.Home;
            iconCurrentFormPictureBox.IconColor = Color.White;
            MainForm mainForm = new MainForm(userlabel.Text.Trim());
            mainForm.Show();
            settingspanel.Hide();


        }
    }
}
