﻿namespace CmmunityManagementSysWinFormsAppV2.WinForm
{
    partial class ExpenseAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.expenseMetroTabControl = new MetroFramework.Controls.MetroTabControl();
            this.addExpMetroTabPage = new MetroFramework.Controls.MetroTabPage();
            this.errorIconPictureBox = new FontAwesome.Sharp.IconPictureBox();
            this.errorlabel = new System.Windows.Forms.Label();
            this.timeComboBox = new MetroFramework.Controls.MetroComboBox();
            this.submitButton = new MetroFramework.Controls.MetroButton();
            this.amountTextBox = new MetroFramework.Controls.MetroTextBox();
            this.htmlLabel4 = new MetroFramework.Drawing.Html.HtmlLabel();
            this.descriptionTextBox = new MetroFramework.Controls.MetroTextBox();
            this.htmlLabel3 = new MetroFramework.Drawing.Html.HtmlLabel();
            this.htmlLabel2 = new MetroFramework.Drawing.Html.HtmlLabel();
            this.dateDateTime = new MetroFramework.Controls.MetroDateTime();
            this.htmlLabel1 = new MetroFramework.Drawing.Html.HtmlLabel();
            this.viewExpMetroTabPage = new MetroFramework.Controls.MetroTabPage();
            this.expenseGrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.viewButton = new MetroFramework.Controls.MetroButton();
            this.toDateTime = new MetroFramework.Controls.MetroDateTime();
            this.htmlLabel6 = new MetroFramework.Drawing.Html.HtmlLabel();
            this.fromDateTime = new MetroFramework.Controls.MetroDateTime();
            this.htmlLabel5 = new MetroFramework.Drawing.Html.HtmlLabel();
            this.expenseMetroTabControl.SuspendLayout();
            this.addExpMetroTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorIconPictureBox)).BeginInit();
            this.viewExpMetroTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expenseGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // expenseMetroTabControl
            // 
            this.expenseMetroTabControl.Controls.Add(this.addExpMetroTabPage);
            this.expenseMetroTabControl.Controls.Add(this.viewExpMetroTabPage);
            this.expenseMetroTabControl.Location = new System.Drawing.Point(2, 0);
            this.expenseMetroTabControl.Name = "expenseMetroTabControl";
            this.expenseMetroTabControl.SelectedIndex = 1;
            this.expenseMetroTabControl.Size = new System.Drawing.Size(828, 514);
            this.expenseMetroTabControl.TabIndex = 0;
            this.expenseMetroTabControl.UseSelectable = true;
            // 
            // addExpMetroTabPage
            // 
            this.addExpMetroTabPage.Controls.Add(this.errorIconPictureBox);
            this.addExpMetroTabPage.Controls.Add(this.errorlabel);
            this.addExpMetroTabPage.Controls.Add(this.timeComboBox);
            this.addExpMetroTabPage.Controls.Add(this.submitButton);
            this.addExpMetroTabPage.Controls.Add(this.amountTextBox);
            this.addExpMetroTabPage.Controls.Add(this.htmlLabel4);
            this.addExpMetroTabPage.Controls.Add(this.descriptionTextBox);
            this.addExpMetroTabPage.Controls.Add(this.htmlLabel3);
            this.addExpMetroTabPage.Controls.Add(this.htmlLabel2);
            this.addExpMetroTabPage.Controls.Add(this.dateDateTime);
            this.addExpMetroTabPage.Controls.Add(this.htmlLabel1);
            this.addExpMetroTabPage.HorizontalScrollbarBarColor = true;
            this.addExpMetroTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.addExpMetroTabPage.HorizontalScrollbarSize = 10;
            this.addExpMetroTabPage.Location = new System.Drawing.Point(4, 38);
            this.addExpMetroTabPage.Name = "addExpMetroTabPage";
            this.addExpMetroTabPage.Size = new System.Drawing.Size(830, 472);
            this.addExpMetroTabPage.TabIndex = 0;
            this.addExpMetroTabPage.Text = "Add Expense";
            this.addExpMetroTabPage.VerticalScrollbarBarColor = true;
            this.addExpMetroTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.addExpMetroTabPage.VerticalScrollbarSize = 10;
            // 
            // errorIconPictureBox
            // 
            this.errorIconPictureBox.BackColor = System.Drawing.Color.White;
            this.errorIconPictureBox.ForeColor = System.Drawing.Color.Red;
            this.errorIconPictureBox.IconChar = FontAwesome.Sharp.IconChar.Exclamation;
            this.errorIconPictureBox.IconColor = System.Drawing.Color.Red;
            this.errorIconPictureBox.IconSize = 30;
            this.errorIconPictureBox.Location = new System.Drawing.Point(25, 433);
            this.errorIconPictureBox.Name = "errorIconPictureBox";
            this.errorIconPictureBox.Size = new System.Drawing.Size(30, 30);
            this.errorIconPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.errorIconPictureBox.TabIndex = 13;
            this.errorIconPictureBox.TabStop = false;
            this.errorIconPictureBox.Visible = false;
            // 
            // errorlabel
            // 
            this.errorlabel.AutoSize = true;
            this.errorlabel.Font = new System.Drawing.Font("Yu Gothic UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorlabel.ForeColor = System.Drawing.Color.DarkGray;
            this.errorlabel.Location = new System.Drawing.Point(59, 433);
            this.errorlabel.Name = "errorlabel";
            this.errorlabel.Size = new System.Drawing.Size(133, 25);
            this.errorlabel.TabIndex = 12;
            this.errorlabel.Text = "Error Message";
            this.errorlabel.Visible = false;
            // 
            // timeComboBox
            // 
            this.timeComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.timeComboBox.FormattingEnabled = true;
            this.timeComboBox.ItemHeight = 29;
            this.timeComboBox.Items.AddRange(new object[] {
            "Current Bill",
            "Gas Bill",
            "Worker Bill",
            "Others"});
            this.timeComboBox.Location = new System.Drawing.Point(162, 92);
            this.timeComboBox.Name = "timeComboBox";
            this.timeComboBox.Size = new System.Drawing.Size(200, 35);
            this.timeComboBox.TabIndex = 11;
            this.timeComboBox.UseSelectable = true;
            // 
            // submitButton
            // 
            this.submitButton.BackColor = System.Drawing.Color.Transparent;
            this.submitButton.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.submitButton.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.submitButton.Location = new System.Drawing.Point(162, 320);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(102, 47);
            this.submitButton.TabIndex = 10;
            this.submitButton.Text = "Submit";
            this.submitButton.UseSelectable = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // amountTextBox
            // 
            // 
            // 
            // 
            this.amountTextBox.CustomButton.Image = null;
            this.amountTextBox.CustomButton.Location = new System.Drawing.Point(160, 2);
            this.amountTextBox.CustomButton.Name = "";
            this.amountTextBox.CustomButton.Size = new System.Drawing.Size(37, 37);
            this.amountTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.amountTextBox.CustomButton.TabIndex = 1;
            this.amountTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.amountTextBox.CustomButton.UseSelectable = true;
            this.amountTextBox.CustomButton.Visible = false;
            this.amountTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.amountTextBox.Lines = new string[0];
            this.amountTextBox.Location = new System.Drawing.Point(162, 263);
            this.amountTextBox.MaxLength = 32767;
            this.amountTextBox.Multiline = true;
            this.amountTextBox.Name = "amountTextBox";
            this.amountTextBox.PasswordChar = '\0';
            this.amountTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.amountTextBox.SelectedText = "";
            this.amountTextBox.SelectionLength = 0;
            this.amountTextBox.SelectionStart = 0;
            this.amountTextBox.ShortcutsEnabled = true;
            this.amountTextBox.Size = new System.Drawing.Size(200, 42);
            this.amountTextBox.TabIndex = 9;
            this.amountTextBox.UseSelectable = true;
            this.amountTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.amountTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // htmlLabel4
            // 
            this.htmlLabel4.AutoScroll = true;
            this.htmlLabel4.AutoScrollMinSize = new System.Drawing.Size(78, 36);
            this.htmlLabel4.AutoSize = false;
            this.htmlLabel4.BackColor = System.Drawing.SystemColors.Window;
            this.htmlLabel4.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.htmlLabel4.Location = new System.Drawing.Point(28, 263);
            this.htmlLabel4.Name = "htmlLabel4";
            this.htmlLabel4.Size = new System.Drawing.Size(111, 49);
            this.htmlLabel4.TabIndex = 8;
            this.htmlLabel4.Text = "Amount";
            // 
            // descriptionTextBox
            // 
            // 
            // 
            // 
            this.descriptionTextBox.CustomButton.Image = null;
            this.descriptionTextBox.CustomButton.Location = new System.Drawing.Point(246, 2);
            this.descriptionTextBox.CustomButton.Name = "";
            this.descriptionTextBox.CustomButton.Size = new System.Drawing.Size(83, 83);
            this.descriptionTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.descriptionTextBox.CustomButton.TabIndex = 1;
            this.descriptionTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.descriptionTextBox.CustomButton.UseSelectable = true;
            this.descriptionTextBox.CustomButton.Visible = false;
            this.descriptionTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.descriptionTextBox.Lines = new string[0];
            this.descriptionTextBox.Location = new System.Drawing.Point(162, 153);
            this.descriptionTextBox.MaxLength = 32767;
            this.descriptionTextBox.Multiline = true;
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.PasswordChar = '\0';
            this.descriptionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.descriptionTextBox.SelectedText = "";
            this.descriptionTextBox.SelectionLength = 0;
            this.descriptionTextBox.SelectionStart = 0;
            this.descriptionTextBox.ShortcutsEnabled = true;
            this.descriptionTextBox.Size = new System.Drawing.Size(332, 88);
            this.descriptionTextBox.TabIndex = 7;
            this.descriptionTextBox.UseSelectable = true;
            this.descriptionTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.descriptionTextBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // htmlLabel3
            // 
            this.htmlLabel3.AutoScroll = true;
            this.htmlLabel3.AutoScrollMinSize = new System.Drawing.Size(107, 36);
            this.htmlLabel3.AutoSize = false;
            this.htmlLabel3.BackColor = System.Drawing.SystemColors.Window;
            this.htmlLabel3.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.htmlLabel3.Location = new System.Drawing.Point(28, 153);
            this.htmlLabel3.Name = "htmlLabel3";
            this.htmlLabel3.Size = new System.Drawing.Size(111, 41);
            this.htmlLabel3.TabIndex = 6;
            this.htmlLabel3.Text = "Description";
            // 
            // htmlLabel2
            // 
            this.htmlLabel2.AutoScroll = true;
            this.htmlLabel2.AutoScrollMinSize = new System.Drawing.Size(47, 36);
            this.htmlLabel2.AutoSize = false;
            this.htmlLabel2.BackColor = System.Drawing.SystemColors.Window;
            this.htmlLabel2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.htmlLabel2.Location = new System.Drawing.Point(28, 92);
            this.htmlLabel2.Name = "htmlLabel2";
            this.htmlLabel2.Size = new System.Drawing.Size(111, 46);
            this.htmlLabel2.TabIndex = 4;
            this.htmlLabel2.Text = "Title";
            // 
            // dateDateTime
            // 
            this.dateDateTime.Location = new System.Drawing.Point(162, 34);
            this.dateDateTime.MinimumSize = new System.Drawing.Size(0, 29);
            this.dateDateTime.Name = "dateDateTime";
            this.dateDateTime.Size = new System.Drawing.Size(200, 29);
            this.dateDateTime.TabIndex = 3;
            // 
            // htmlLabel1
            // 
            this.htmlLabel1.AutoScroll = true;
            this.htmlLabel1.AutoScrollMinSize = new System.Drawing.Size(50, 36);
            this.htmlLabel1.AutoSize = false;
            this.htmlLabel1.BackColor = System.Drawing.SystemColors.Window;
            this.htmlLabel1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.htmlLabel1.Location = new System.Drawing.Point(28, 34);
            this.htmlLabel1.Name = "htmlLabel1";
            this.htmlLabel1.Size = new System.Drawing.Size(111, 46);
            this.htmlLabel1.TabIndex = 2;
            this.htmlLabel1.Text = "Date";
            // 
            // viewExpMetroTabPage
            // 
            this.viewExpMetroTabPage.Controls.Add(this.expenseGrid);
            this.viewExpMetroTabPage.Controls.Add(this.viewButton);
            this.viewExpMetroTabPage.Controls.Add(this.toDateTime);
            this.viewExpMetroTabPage.Controls.Add(this.htmlLabel6);
            this.viewExpMetroTabPage.Controls.Add(this.fromDateTime);
            this.viewExpMetroTabPage.Controls.Add(this.htmlLabel5);
            this.viewExpMetroTabPage.HorizontalScrollbarBarColor = true;
            this.viewExpMetroTabPage.HorizontalScrollbarHighlightOnWheel = false;
            this.viewExpMetroTabPage.HorizontalScrollbarSize = 10;
            this.viewExpMetroTabPage.Location = new System.Drawing.Point(4, 38);
            this.viewExpMetroTabPage.Name = "viewExpMetroTabPage";
            this.viewExpMetroTabPage.Size = new System.Drawing.Size(820, 472);
            this.viewExpMetroTabPage.TabIndex = 1;
            this.viewExpMetroTabPage.Text = "View Expense";
            this.viewExpMetroTabPage.VerticalScrollbarBarColor = true;
            this.viewExpMetroTabPage.VerticalScrollbarHighlightOnWheel = false;
            this.viewExpMetroTabPage.VerticalScrollbarSize = 10;
            // 
            // expenseGrid
            // 
            this.expenseGrid.AllowUserToAddRows = false;
            this.expenseGrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Yu Gothic UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.expenseGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.expenseGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.expenseGrid.BackgroundColor = System.Drawing.Color.White;
            this.expenseGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.expenseGrid.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.expenseGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.expenseGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.expenseGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.expenseGrid.DefaultCellStyle = dataGridViewCellStyle8;
            this.expenseGrid.GridColor = System.Drawing.Color.White;
            this.expenseGrid.Location = new System.Drawing.Point(-4, 56);
            this.expenseGrid.Name = "expenseGrid";
            this.expenseGrid.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.expenseGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.expenseGrid.RowHeadersWidth = 50;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Yu Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            this.expenseGrid.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.expenseGrid.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Yu Gothic UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expenseGrid.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
            this.expenseGrid.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.expenseGrid.RowTemplate.Height = 30;
            this.expenseGrid.Size = new System.Drawing.Size(824, 403);
            this.expenseGrid.TabIndex = 24;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Date";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Title";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Description";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Amount";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // viewButton
            // 
            this.viewButton.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.viewButton.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.viewButton.Location = new System.Drawing.Point(576, 18);
            this.viewButton.Name = "viewButton";
            this.viewButton.Size = new System.Drawing.Size(133, 32);
            this.viewButton.TabIndex = 15;
            this.viewButton.Text = "View";
            this.viewButton.UseSelectable = true;
            this.viewButton.Click += new System.EventHandler(this.viewButton_Click);
            // 
            // toDateTime
            // 
            this.toDateTime.Location = new System.Drawing.Point(367, 18);
            this.toDateTime.MinimumSize = new System.Drawing.Size(0, 29);
            this.toDateTime.Name = "toDateTime";
            this.toDateTime.Size = new System.Drawing.Size(200, 29);
            this.toDateTime.TabIndex = 13;
            this.toDateTime.Value = new System.DateTime(2020, 2, 16, 0, 0, 0, 0);
            // 
            // htmlLabel6
            // 
            this.htmlLabel6.AutoScroll = true;
            this.htmlLabel6.AutoScrollMinSize = new System.Drawing.Size(30, 36);
            this.htmlLabel6.AutoSize = false;
            this.htmlLabel6.BackColor = System.Drawing.SystemColors.Window;
            this.htmlLabel6.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.htmlLabel6.Location = new System.Drawing.Point(318, 15);
            this.htmlLabel6.Name = "htmlLabel6";
            this.htmlLabel6.Size = new System.Drawing.Size(75, 44);
            this.htmlLabel6.TabIndex = 12;
            this.htmlLabel6.Text = "To";
            // 
            // fromDateTime
            // 
            this.fromDateTime.Location = new System.Drawing.Point(95, 15);
            this.fromDateTime.MinimumSize = new System.Drawing.Size(0, 29);
            this.fromDateTime.Name = "fromDateTime";
            this.fromDateTime.Size = new System.Drawing.Size(200, 29);
            this.fromDateTime.TabIndex = 11;
            this.fromDateTime.Value = new System.DateTime(2020, 2, 16, 0, 0, 0, 0);
            // 
            // htmlLabel5
            // 
            this.htmlLabel5.AutoScroll = true;
            this.htmlLabel5.AutoScrollMinSize = new System.Drawing.Size(54, 36);
            this.htmlLabel5.AutoSize = false;
            this.htmlLabel5.BackColor = System.Drawing.SystemColors.Window;
            this.htmlLabel5.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.htmlLabel5.Location = new System.Drawing.Point(9, 15);
            this.htmlLabel5.Name = "htmlLabel5";
            this.htmlLabel5.Size = new System.Drawing.Size(80, 44);
            this.htmlLabel5.TabIndex = 10;
            this.htmlLabel5.Text = "From";
            // 
            // ExpenseAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(832, 504);
            this.Controls.Add(this.expenseMetroTabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ExpenseAdd";
            this.Text = "ExpenseAdd";
            this.Load += new System.EventHandler(this.ExpenseAdd_Load);
            this.expenseMetroTabControl.ResumeLayout(false);
            this.addExpMetroTabPage.ResumeLayout(false);
            this.addExpMetroTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorIconPictureBox)).EndInit();
            this.viewExpMetroTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expenseGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl expenseMetroTabControl;
        private MetroFramework.Controls.MetroTabPage addExpMetroTabPage;
        private MetroFramework.Controls.MetroComboBox timeComboBox;
        private MetroFramework.Controls.MetroButton submitButton;
        private MetroFramework.Controls.MetroTextBox amountTextBox;
        private MetroFramework.Drawing.Html.HtmlLabel htmlLabel4;
        private MetroFramework.Controls.MetroTextBox descriptionTextBox;
        private MetroFramework.Drawing.Html.HtmlLabel htmlLabel3;
        private MetroFramework.Drawing.Html.HtmlLabel htmlLabel2;
        private MetroFramework.Controls.MetroDateTime dateDateTime;
        private MetroFramework.Drawing.Html.HtmlLabel htmlLabel1;
        private MetroFramework.Controls.MetroTabPage viewExpMetroTabPage;
        private MetroFramework.Controls.MetroButton viewButton;
        private MetroFramework.Controls.MetroDateTime toDateTime;
        private MetroFramework.Drawing.Html.HtmlLabel htmlLabel6;
        private MetroFramework.Controls.MetroDateTime fromDateTime;
        private MetroFramework.Drawing.Html.HtmlLabel htmlLabel5;
        private System.Windows.Forms.DataGridView expenseGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private FontAwesome.Sharp.IconPictureBox errorIconPictureBox;
        private System.Windows.Forms.Label errorlabel;
    }
}