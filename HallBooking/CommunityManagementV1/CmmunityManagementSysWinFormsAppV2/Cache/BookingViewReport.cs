﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CmmunityManagementSysWinFormsAppV2.Cache
{
    class BookingViewReport
    {
       
        public string Date { get; set; }
        public int TotalAmount { get; set; }
        public int PaidAmount { get; set; }
        public int DueAmount { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string Purpose { get; set; }
        public string Status { get; set; }
        public string start { get; set; }
        public string end { get; set; }

    }
}
